/*
 * CProgram1.c
 *
 * Created: 5/6/2011 1:07:15 AM
 *  Author: Administrator
 * Modified: 06/2014 R.Velarde.. Release 0.1 with dual channel frequency agility
 * Modified: 01/2015 R.Velarde.. Eliminate noisy non-system packets, added "bad packet padding"
 */ 

#include "main.h"


ISR(INT0_vect)									// Source selection button
{
	cli();										// Disable Interrupts
	
	if (SOURCE > 0)
	{
		SOURCE--;
	}
	else
	{
		SOURCE = 3;
	}
	
	source_led_sel(SOURCE);
	source_sel();
	
	sei();										// Enable Interrupts
}

ISR(INT4_vect)									// MCUI_SOURCE_SELA Update
{
	cli();										// Disable Interrupts
	
	for (unsigned int i = 0; i < 10000; i++);	// 2.5msec Delay for debounce
	for (unsigned int i = 0; i < 10000; i++);	// 2.5msec Delay for debounce
	for (unsigned int i = 0; i < 10000; i++);	// 2.5msec Delay for debounce
	for (unsigned int i = 0; i < 10000; i++);	// 2.5msec Delay for debounce
	
	if      (((PINE >> 4) & 0x01) == 0)			// Check that GPIO is still selected
	{
		SOURCE = TC99;							// Change Source variable
		
		source_led_sel(SOURCE);					// Change LED to correct source
		source_sel();							// Call Source Select function
	}

	sei();										// Enable Interrupts
}

ISR(INT5_vect)									// MCUI_SOURCE_SELB Update
{
	cli();										// Disable Interrupts
	
	for (unsigned int i = 0; i < 10000; i++);	// 2.5msec Delay for debounce
	for (unsigned int i = 0; i < 10000; i++);	// 2.5msec Delay for debounce
	for (unsigned int i = 0; i < 10000; i++);	// 2.5msec Delay for debounce
	for (unsigned int i = 0; i < 10000; i++);	// 2.5msec Delay for debounce
	
	if      (((PINE >> 5) & 0x01) == 0)			// Check that GPIO is still selected
	{
		SOURCE = I131;							// Change Source variable
	
		source_led_sel(SOURCE);					// Change LED to correct source
		source_sel();							// Call Source Select function
	}		
	
	sei();										// Enable Interrupts
}

ISR(INT6_vect)									// MCUI_SOURCE_SELC Update
{
	cli();										// Disable Interrupts
	
	for (unsigned int i = 0; i < 10000; i++);	// 2.5msec Delay for debounce
	for (unsigned int i = 0; i < 10000; i++);	// 2.5msec Delay for debounce
	for (unsigned int i = 0; i < 10000; i++);	// 2.5msec Delay for debounce
	for (unsigned int i = 0; i < 10000; i++);	// 2.5msec Delay for debounce
	
	if      (((PINE >> 6) & 0x01) == 0)			// Check that GPIO is still selected
	{
		SOURCE = I125;							// Change Source variable
					
		source_led_sel(SOURCE);					// Change LED to correct source
		source_sel();							// Call Source Select function
	}		
	
	sei();										// Enable Interrupts
}

ISR(INT7_vect)									// MCUI_SOURCE_SELD Update
{
	cli();										// Disable Interrupts
	
	for (unsigned int i = 0; i < 10000; i++);	// 2.5msec Delay for debounce
	for (unsigned int i = 0; i < 10000; i++);	// 2.5msec Delay for debounce
	for (unsigned int i = 0; i < 10000; i++);	// 2.5msec Delay for debounce
	for (unsigned int i = 0; i < 10000; i++);	// 2.5msec Delay for debounce
	
	if      (((PINE >> 7) & 0x01) == 0)			// Check that GPIO is still selected
	{
		SOURCE = IN111;							// Change Source variable
					
		source_led_sel(SOURCE);					// Change LED to correct source
		source_sel();							// Call Source Select function
	}		
	
	sei();										// Enable Interrupts
}

ISR(TIMER1_COMPA_vect)
{
	TIMER_ISR			=	1;
}

ISR(TRX24_RX_END_vect)
{
	RF_RX_END_ISR		=	1;
}

ISR(TRX24_TX_END_vect)
{
	RF_TX_END_ISR		=	1;
}

void source_sel(void)
{
	switch (SOURCE)
	{
		case 3:
			dpot_set(SRC0_AMPLITUDE);								// Change amplitude of pulse train to BS
			break;
		case 2:
			dpot_set(SRC1_AMPLITUDE);								// Change amplitude of pulse train to BS
			break;
		case 1:
			dpot_set(SRC2_AMPLITUDE);								// Change amplitude of pulse train to BS
			break;
		case 0:
			dpot_set(SRC3_AMPLITUDE);								// Change amplitude of pulse train to BS
			break;
		default:
			break;
	}
	
	PROBE_CAL_SETTINGS_UPDATE	= 1;								// Set PROBE_CAL_SETTINGS_UPDATE flag
	RF_RESEND_CAL_SETTINGS		= 0;								// Clear RF Resend Counter
}

void rf_rx(void)
{
	RF_ERROR	= 1;												// Clear RF_ERROR flag
	
	if(PHY_RSSI & (1 << RX_CRC_VALID))
	{
		if ((_SFR_MEM8(0x180) == 0x1D) && (_SFR_MEM8(0x181) == 0xDA))						// Data Header Check
		{
			if(_SFR_MEM8(0x182) == 0xFF)								// Verify is a Probe-to-dongle message
			{
				MSG_TYPE	= _SFR_MEM8(0x183);								// read value
				MSG_DATA1	= _SFR_MEM8(0x184);								//  read value
				MSG_DATA2	= _SFR_MEM8(0x185);								//  read value
				RF_ERROR	= 0;
			}
		
		}
		else															// If Redundant Characters do NOT match...
		{
			RF_MSG_TYPE_ERROR++;										// Increment error counter
			RF_MSG_DATA1_ERROR++;										// Increment error counter
			RF_MSG_DATA2_ERROR++;										// Increment error counter
		
			RF_ERROR	= 1;											// Set RF_ERROR flag
		}
	}

	
	
	LQI				= _SFR_MEM8(0x189);								// Read Link Quality Indicator value
	
	if (RF_ERROR)
	{
		MSG_TYPE	= 0;											// If any of the 3 redundant character checks fail, clear values
		MSG_DATA1	= 0;											//
		MSG_DATA2	= 0;											//
		//BAD_PACKET++;												// Increment consecutive Bad Packet Counter
		MISSED_PACKET	= 0;										// Reset consecutive Missed Packet counter
	}
	else
	{
		switch (MSG_TYPE)
		{
			case MSG_COUNT:											// Count Message
				NEW_COUNT_MSG	= 1;								// Set NEW_COUNT_MSG flag
				BAD_PACKET		= 0;								// Reset consecutive Bad Packet Counter
				MISSED_PACKET	= 0;								// Reset consecutive Missed Packet counter
				break;
				
			case MSG_ACK:
				BAD_PACKET		= 0;								// Reset consecutive Bad Packet Counter
				MISSED_PACKET	= 0;								// Reset consecutive Missed Packet counter	
				if(MSG_DATA1 == 0x0A)
				{
						PROBE_CAL_SETTINGS_UPDATE = 1;				//Request for settings
				}
				else if(MSG_DATA1 == 0x0C)
				{
						PROBE_CHANNEL_SW = 1;						//Probe acknowledged channel switch	
						RF_CHANNEL_HOP	 = 0;				    // Clear RF_CHANNEL_HOP flag
				}
			    break;
			
			default:
				MSG_TYPE_ERROR++;									// Increment error counter
				MSG_TYPE	= 0;									// If the data is not counts, clear values
				MSG_DATA1	= 0;									//
				MSG_DATA2	= 0;									//
				BAD_PACKET++;										// Increment consecutive Bad Packet Counter
				MISSED_PACKET	= 0;								// Reset consecutive Missed Packet counter
				break;
		}
	}
}

void rf_tx(unsigned char MSG_TYPE, unsigned char MSG_DATA1, unsigned char MSG_DATA2)
{
	TRX_STATE	=	PLL_ON;											// RF Transceiver to RX_ON mode (Rx Listen State)
	for (int i = 0; i < 20; i++);									// 5us Delay (1us required for transition)
	
	TRXFBST				=	0x09;
	_SFR_MEM8(0x181)	=	0x1D;
	_SFR_MEM8(0x182)	=	0xDA;
	_SFR_MEM8(0x183)	=	0x00;
	_SFR_MEM8(0x184)	=	MSG_TYPE;
	_SFR_MEM8(0x185)	=	MSG_DATA1;
	_SFR_MEM8(0x186)	=	MSG_DATA2;
	
	TRX_STATE	=	CMD_TX_START;									// RF Transceiver to BUSY_TX mode (Transmit State)
}

void source_led_blink(unsigned char LED, unsigned char RATE)
{
	for (unsigned int i = 0; i < 20; i++)
	{
		source_led_sel(LED);
						
		for (unsigned int i = 0; i < RATE; i++)
		{
			for (unsigned int i = 0; i < 10000; i++);				// 2.5msec Delay
		};
						
		source_led_off();
						
		for (unsigned int i = 0; i < RATE; i++)
		{
			for (unsigned int i = 0; i < 10000; i++);				// 2.5msec Delay
		};
	};
}

void pulse_transfer(void)
{
	for(int i = 0; i < 15; i++)
	{
		PULSE_BUFFER[i+1]	=	PULSE_BUFFER[i];						// Shifts contents of each array value to the next slot (leaving slot 0 ready for write)
	};
				
	PULSE_BUFFER[0]			=	PULSE_COUNT;							// Write new PULSE_COUNT value into slot 0 of array
	PULSE_BUFFER_SUM		=	0;										// Clear the previous PULSE_BUFFER_SUM value
				
	for(int i = 0; i < 16; i++)
	{
		PULSE_BUFFER_SUM	+=	PULSE_BUFFER[i];						// Sum all values in array
	};
				
	PULSE_BUFFER_AVG		=	(PULSE_BUFFER_SUM >> 4) & 0x0FFF;		// Divide sum by 16 to get average value (25msec * 16 = 400msec window)
	BITBANG_COUNT			=	PULSE_BUFFER_AVG;						// Load averaged pulse count into BitBang register
				
	BITBANG_COUNT =	BITBANG_COUNT << 1;
	for (BITBANG_COUNT; BITBANG_COUNT > 0; BITBANG_COUNT--)
	{
		PORTB	^=	(1 << PORTB2);							// MCUO_COUNT_DATA_FB 'toggle'
		for (int i = 0; i < 18; i++);
	};
}



int main(void)
{
	MCUSR = 0;
	wdt_disable();													// Disable WDT
	cli();															// Disable Interrupts
	
	/////////////////////////////////////////
	// Initialize GPIO, Timer 1, and LED's //
	/////////////////////////////////////////
	init_gpio();
	init_timer1();
	led_off();
	
	/////////////////////////////////
	// LED turn-on display routine //
	/////////////////////////////////
	for (unsigned int i = 0; i < 4; i++)
	{
		source_led_on();
		
		for (unsigned int i = 0; i < 40; i++)
		{
			for (unsigned int i = 0; i < 10000; i++);				// 2.5msec Delay
		};
		
		source_led_off();
				
		for (unsigned int i = 0; i < 40; i++)
		{
			for (unsigned int i = 0; i < 10000; i++);				// 2.5msec Delay
		};
	};
	
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	// If 'Source Selector Switch' is connected, change to selected SOURCE, otherwise set source to default //
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	for (unsigned int i = 0; i < 10000; i++);						// 2.5msec Delay for debounce
	for (unsigned int i = 0; i < 10000; i++);						// 2.5msec Delay for debounce
	for (unsigned int i = 0; i < 10000; i++);						// 2.5msec Delay for debounce
	for (unsigned int i = 0; i < 10000; i++);						// 2.5msec Delay for debounce
	
	if      (((PINE >> 4) & 0x01) == 0)
	{
		SOURCE = TC99;
	}
	else if	(((PINE >> 5) & 0x01) == 0)
	{
		SOURCE = I131;
	}
	else if	(((PINE >> 6) & 0x01) == 0)
	{
		SOURCE = I125;
	}
	else if	(((PINE >> 7) & 0x01) == 0)
	{
		SOURCE = IN111;
	}
	
	source_led_sel(SOURCE);											// Turn 'ON' LED for Source
	source_sel();													// Call source_sel() function
	
	///////////////////////////////
	// Initialize RF Transceiver //
	///////////////////////////////
	TRX_CTRL_2	&=	~(1 << OQPSK_DATA_RATE1) & ~(1 << OQPSK_DATA_RATE0);	// Configure RF Transceiver for 250kbps Data Rate
	IRQ_MASK	|=	(1 << RX_END_EN) | (1 << TX_END_EN);			// Enable RF interrupts
	
	TRX_STATE	= TRX_OFF;											// RF Transceiver to TRX_OFF mode (XOSC=ON)
	for (int i = 0; i < 2000; i++);									// 500us Delay (240us required for transition)
	
	for (int i = 0; i <= 127; i++)
	{
		_SFR_MEM8(0x180 + i)=	0x00;								// Clear RF data buffer
	}
	
	///////////////////////////////////////////////////////////
	// EEPROM Read/Write routine for RF_CHANNEL and RF_GROUP //
	///////////////////////////////////////////////////////////
	if (eeprom_read_byte(EEPROM_EMPTY) == 0xFF)
	{
		eeprom_write_byte(EEPROM_RF_CHANNEL,RF_CHANNEL);			// Commit RF_CHANNEL to EEPROM
		eeprom_write_byte(EEPROM_EMPTY,0);							// Clear EEPROM_EMPTY byte in EEPROM
	}
	else if(eeprom_read_byte(EEPROM_RF_CHANNEL) != 0x0F)
	{
		eeprom_write_byte(EEPROM_RF_CHANNEL,RF_CHANNEL);			// Commit RF_CHANNEL to EEPROM	
	}
	else
	{
		RF_CHANNEL	= eeprom_read_byte(EEPROM_RF_CHANNEL);			// Read RF_CHANNEL byte from EEPROM
	}
	
	/////////////////////////////////////////////////////////////
	// Enable RF Transceiver and configure for RF Receive mode //
	/////////////////////////////////////////////////////////////
	PHY_CC_CCA	= RF_CHANNEL;										// Change RF Channel
	TRX_STATE	= RX_ON;											// RF Transceiver to RX_ON mode (Rx Listen State) 
	for (int i = 0; i < 1000; i++);									// 250us Delay (110us required for transition)
	
	PHY_CC_CCA	|=	(1 << CCA_REQUEST);								// Initiate Clear Channel Assessment
	while ( !( IRQ_STATUS & (1 << CCA_ED_DONE)) );					// Wait for Clear Channel Assessment to complete
	IRQ_STATUS |=	(1 << CCA_ED_DONE);								// Clear CCA_ED_DONE IRQ
	
	sei();															// Enable Interrupts
		
while (1)
	{
		////////////////////
		// RF Receive ISR //
		////////////////////
		if (RF_RX_END_ISR)
		{
			cli();													// Disable Interrupts
			wdt_disable();											// Disable WDT
			RF_RX_END_ISR	=	0;									// Clear ISR Flag
			
			timer1_enable(0);										// Clear Timer 1
			
			///////////////////////////////
			// RF LED Blink 'ON' Routine //
			///////////////////////////////
			LED_BLINK_RATE++;
						
			if (LED_BLINK_RATE > ( (LED_BLINK_RATE_MAX >> 1) & 0x7F) )
			{
				led_on();											// LED On
			}
			if (LED_BLINK_RATE > LED_BLINK_RATE_MAX)
			{
				LED_BLINK_RATE = 0;
			}
			
			//////////////////////////////
			// RF Receive Function Call //
			//////////////////////////////
			rf_rx();												// CALL RF Receive function
			for (int i = 0; i < 1000; i++);							// 250us Delay before RF Transmit of response message
			
			if (PROBE_CHANNEL_SW)
			{
				PROBE_CHANNEL_SW	= 0;							//Clear Probe acknowledge channel switch
				
				PHY_CC_CCA			= RF_CHANNEL;					// Change RF Channel
			}

			
			/////////////////////////////////
			// Start RF Link Quality Check //
			/////////////////////////////////
			CURRENT_RSSI	= PHY_RSSI & 0x1F;						// RSSI value check
			if ((LQI < 100) && (RF_ERROR == 0))											// If poor link quality, go to next channel
			//if (LQI < 100)
			{
				BAD_CHANNEL_COUNT++;
				
				if(BAD_CHANNEL_COUNT > 4)							// Only change channels after 10 low RSSI readings (10*25msec => 250msec)
				{
					BAD_CHANNEL_COUNT	= 0;							// Reset the counter
					
					if(RF_CHANNEL == 15)
					{
						RF_CHANNEL = 20;
					}
					else
					{
						RF_CHANNEL = 15;
					}
					
					RF_CHANNEL_HOP	= 1;							// Set RF_CHANNEL_HOP flag
				}
			}
			else
			{
				if(BAD_CHANNEL_COUNT > 0)
				{
					BAD_CHANNEL_COUNT--;
				}
			}
			
			////////////////////////////////////////////////////////////////////////////
			// RF Transmit Function Call (CHANNGEL_SET, CAL_SETTINGS, or ACK Message) //
			////////////////////////////////////////////////////////////////////////////
			//sei();													// ENABLE Interrupts
			
			
			if(RF_ERROR == 0)
			{
					MISSED_PACKET = 0;
					if (RF_CHANNEL_HOP)										// NOTE: RF_CHANNEL_HOP is cleared in RF_TX_END_ISR
					{
							RF_RESEND_CH_UPDATE++;							// Increment RF Resend Counter
				
							if(RF_RESEND_CH_UPDATE > 2)						// Resend message 10 times, before clearing flags
							{
									RF_RESEND_CH_UPDATE		= 0;				// Clear RF Resend Counter
									if (PROBE_CHANNEL_SW)
									{
										PROBE_CHANNEL_SW	= 0;							//Clear Probe acknowledge channel switch
										
										PHY_CC_CCA			= RF_CHANNEL;					// Change RF Channel
									}
							}
									
							//eeprom_write_byte(EEPROM_RF_CHANNEL,RF_CHANNEL);	// Commit RF_CHANNEL to EEPROM
				
							rf_tx(MSG_CHANNEL_SET,RF_CHANNEL,0);				// Transmit RF Channel Configuration Message
					
					}
					else if (PROBE_CAL_SETTINGS_UPDATE)
					{
						RF_RESEND_CAL_SETTINGS++;							// Increment RF Resend Counter
				
						if(RF_RESEND_CAL_SETTINGS > 4)						// Resend message 10 times, before clearing flags
						{
							PROBE_CAL_SETTINGS_UPDATE	= 0;				// Clear PROBE_CAL_SETTINGS_UPDATE flag
							RF_RESEND_CAL_SETTINGS		= 0;				// Clear RF Resend Counter
						}
				
						rf_tx(MSG_CAL_SETTINGS,SOURCE,0);					// Transmit Probe Configuration Message
					}
					else
					{
						rf_tx(MSG_ACK,0,0);									// Transmit ACK Message
					}
			}
			sei();													// ENABLE Interrupts
		}
		
		/////////////////////
		// RF Transmit ISR //
		/////////////////////
		if (RF_TX_END_ISR)
		{
			cli();													// Disable Interrupts
			wdt_reset();											// Reset WDT
			RF_TX_END_ISR	= 0;									// Clear ISR Flag
			
			////////////////////////////////
			// RF LED Blink 'OFF' Routine //
			////////////////////////////////
			if (LED_BLINK_RATE < ( (LED_BLINK_RATE_MAX >> 1) & 0x7F) )
			{
				led_off();											// LED Off
			}
			
			/////////////////////////////////////////
			// RF Transceiver to 'Rx Listen State' //
			/////////////////////////////////////////
			TRX_STATE	= RX_ON;									// RF Transceiver to RX_ON mode (Rx Listen State)
			for (int i = 0; i < 20; i++);							// 5us Delay (1us required for transition)
			
			/////////////////////////////////////////////////////////////////////////////
			// If CHANNGEL_SET message was sent, then clear flag and change RF Channel //
			/////////////////////////////////////////////////////////////////////////////
			if (PROBE_CHANNEL_SW)
			{
				PROBE_CHANNEL_SW	= 0;							//Clear Probe acknowledge channel switch
				
				PHY_CC_CCA			= RF_CHANNEL;					// Change RF Channel
			}
			
			//MISSED_PACKET = 0;
			timer1_enable(0);										// Clear Timer 1
			timer1_enable(1);										// Start Timer 1
			
			////////////////////////////////////
			// Start 'Pulse Transfer' routine //
			////////////////////////////////////
			//PULSE_COUNT = 0;
			
			if(NEW_COUNT_MSG)
			{
				NEW_COUNT_MSG = 0;
				
				PULSE_COUNT				=	0x0000;
				PULSE_COUNT				|=	MSG_DATA2			&	0x00FF;
				PULSE_COUNT				=	(PULSE_COUNT << 8)	&	0xFF00;
				PULSE_COUNT				|=	MSG_DATA1			&	0x00FF;
				
				
				
							// TEST CODE!!!!!!!!!!!!
							//PULSE_COUNT =	  25;/////////////// 1000 COUNTS
							//PULSE_COUNT =	 125;/////////////// 5000 COUNTS
							//PULSE_COUNT =	 625;///////////////25000 COUNTS
							//PULSE_COUNT =	1250;///////////////50000 COUNTS
							//PULSE_COUNT =	2000;///////////////80000 COUNTS
				
				
				
				
				LAST_PULSE_COUNT		=	PULSE_COUNT;
				pulse_transfer();
			}
			else
			{
				//PULSE_COUNT		= 0;								// Pad count FIFO with 0 counts
				//LAST_PULSE_COUNT	= 0;							// Reset to 0
				
				if(BAD_PACKET > 2)
				{
					if(BAD_PACKET > 8)
					{
						if(BAD_PACKET > 12)
						{
							if(BAD_PACKET > 16)
							{
								PROBE_CHANNEL_SW	= 0;							//Clear Probe acknowledge channel switch
								RF_CHANNEL_HOP		= 0;							// Clear RF_CHANNEL_HOP flag
								PHY_CC_CCA			= 15;
								PROBE_CAL_SETTINGS_UPDATE = 1;				   //Request for settings
								/////////////////////////////////////////
								// RF Transceiver to 'Rx Listen State' //
								/////////////////////////////////////////
								TRX_STATE	= RX_ON;									// RF Transceiver to RX_ON mode (Rx Listen State)
								for (int i = 0; i < 20; i++);							// 5us Delay (1us required for transition)
								
								BAD_PACKET		= 0;								// Reset consecutive Missed Packet counter
								PULSE_COUNT		= 0;								// Pad count FIFO with 0 counts
								LAST_PULSE_COUNT	= 0;							// Reset to 0
							}
							else
							{
								PULSE_COUNT		= 0;								// Pad count FIFO with 0 counts when more than 12 consecutive packets are missed
							}
						}
						else
						{
							PULSE_COUNT		=	(LAST_PULSE_COUNT >> 2) & 0x3FFF;	// Pad count FIFO with (Last Good Count)/4 when 9-12 consecutive packets are missed
						}
					}
					else
					{
						PULSE_COUNT		=	(LAST_PULSE_COUNT >> 1) & 0x7FFF;			// Pad count FIFO with (Last Good Count)/2 when 3-8 consecutive packets are missed
					}
				}
				else
				{
					PULSE_COUNT		=	LAST_PULSE_COUNT;							// Pad count FIFO with (Last Good Count) when 0-2 consecutive packets are missed
				}
				pulse_transfer();
			}			
			
			
			
			
			
			wdt_enable(WDTO_4S);									// Enable WDT
			sei();													// Enable Interrupts
		}
		
		//////////////////////
		// 25msec Timer ISR //
		//////////////////////
		if (TIMER_ISR)
		{
			cli();													// Disable Interrupts
			wdt_reset();											// Reset WDT
			TIMER_ISR	= 0;										// Clear ISR Flag
			
			timer1_enable(0);										// Clear Timer 1
			timer1_enable(1);										// Start Timer 1

			MISSED_PACKET++;										// Increment consecutive Missed Packet counter
			
			////////////////////////////////////////////////////////////////////////////////
			// Adaptively Pad the PULSE BUFFER array, then start 'Pulse Transfer' routine //
			////////////////////////////////////////////////////////////////////////////////
			//if(MISSED_PACKET > 2)
			//{
				/*if (PROBE_CHANNEL_SW)
				{
					PROBE_CHANNEL_SW	= 0;							//Clear Probe acknowledge channel switch
					RF_CHANNEL_HOP		= 0;							// Clear RF_CHANNEL_HOP flag
					PHY_CC_CCA			= RF_CHANNEL;					// Change RF Channel
				}*/
				
			//}
			if(MISSED_PACKET > 4)
			{
				//PULSE_COUNT	= 0;
				//LAST_PULSE_COUNT	= 0;
				if(MISSED_PACKET > 8)
				{
					if(MISSED_PACKET > 12)
					{
						//if(MISSED_PACKET > 14)
						//{
							if(MISSED_PACKET > 16)
							{
								if(MISSED_PACKET > 32)
								{
									//MISSED_PACKET = 0;
									PROBE_CHANNEL_SW	= 0;							//Clear Probe acknowledge channel switch
									RF_CHANNEL_HOP		= 0;							// Clear RF_CHANNEL_HOP flag
									PHY_CC_CCA			= 15;
									PROBE_CAL_SETTINGS_UPDATE = 1;				   //Request for settings
									/////////////////////////////////////////
									// RF Transceiver to 'Rx Listen State' //
									/////////////////////////////////////////
									TRX_STATE	= RX_ON;									// RF Transceiver to RX_ON mode (Rx Listen State)
									for (int i = 0; i < 20; i++);							// 5us Delay (1us required for transition)
								}						
								MISSED_PACKET	= 0;								// Reset consecutive Missed Packet counter
								PULSE_COUNT		= 0;								// Pad count FIFO with 0 counts
								LAST_PULSE_COUNT	= 0;							// Reset to 0
							}						
							else
							{
								PULSE_COUNT		= 0;								// Pad count FIFO with 0 counts when more than 12 consecutive packets are missed
								//PULSE_COUNT		=	(LAST_PULSE_COUNT >> 1) & 0x3FFF;
							}
						//}
						//else
						//{
							//PULSE_COUNT		=	(LAST_PULSE_COUNT >> 3) & 0x1FFF;
							//PULSE_COUNT		=	(LAST_PULSE_COUNT >> 2) & 0x3FFF;	
						//}
					}
					else
					{
						PULSE_COUNT		=	(LAST_PULSE_COUNT >> 2) & 0x3FFF;	// Pad count FIFO with (Last Good Count)/4 when 9-12 consecutive packets are missed
						//PULSE_COUNT		=	(LAST_PULSE_COUNT >> 1) & 0x7FFF;
					}
				}
				else
				{
					PULSE_COUNT		=	(LAST_PULSE_COUNT >> 1) & 0x7FFF;			// Pad count FIFO with (Last Good Count)/2 when 3-8 consecutive packets are missed
					//PULSE_COUNT		=	LAST_PULSE_COUNT;
				}
			}
			else
			{
				PULSE_COUNT		=	LAST_PULSE_COUNT;							// Pad count FIFO with (Last Good Count) when 0-2 consecutive packets are missed
				//PULSE_COUNT		= 0;
			}
			
			//PULSE_COUNT		= 0;
		    pulse_transfer();
			
			
			wdt_enable(WDTO_4S);									// Enable WDT
			sei();													// Enable Interrupts
		}
	}		
}