/*
 * IncFile1.h
 *
 * Created: 5/5/2011 9:22:26 PM
 *  Author: Administrator
 */ 


#ifndef CUSTOM_FUNCTIONS_H_
#define CUSTOM_FUNCTIONS_H_

#include "common_constants.h"


#define F_CPU 16000000UL

void init_gpio(void);

void led_toggle(void);
void led_on(void);
void led_off(void);
void source_led_sel(unsigned char SOURCE_SEL);
void source_led_on(void);
void source_led_off(void);

void count_tx(unsigned char cData);

void dpot_set(unsigned char DPOT_POSITION);

void init_timer1(void);
void timer1_enable(unsigned char ENABLE);


#endif /* CUSTOM_FUNCTIONS_H_ */