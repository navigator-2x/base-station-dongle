#include "avr/io.h"
#include "avr/interrupt.h"

#include "custom_functions.h"

/*
 * CProgram1.c
 *
 * Created: 5/5/2011 9:22:22 PM
 *  Author: Administrator
 */ 


/* GPIO Initialization Routine
 * Port B, Port D, Port E, Port F, and Port G only.
 * Port A and Port C are NOT used in ATmega128RFA1 Device.
 */

void init_gpio(void)
{
	MCUCR	&=	~(1 << PUD);							// Enable internal pull-up resistors, globally
	
	DPDS0	=	0x00;									// Port F, E, D, and B drive strength all set = 2mA
	DPDS1	=	0x00;									// Port G drive strength set = 2mA
	
	DDRB	=	0xFF;									// Port B configured as output on pins 0-7
	DDRD	=	0xFF;									// Port D configured as output on pins 0-7
	DDRE	=	0xFF;									// Port E configured as output on pins 0-7
	DDRF	=	0xFF;									// Port F configured as output on pins 0-7
	DDRG	=	0xFF;									// Port G configured as output on pins 0-7
	
	PORTB	=	0x00;									// Port B configured as logic low or pull-up resistor off on pins 0-7
	PORTD	=	0x00;									// Port D configured as logic low or pull-up resistor off on pins 0-7
	PORTE	=	0x00;									// Port E configured as logic low or pull-up resistor off on pins 0-7
	PORTF	=	0x00;									// Port F configured as logic low or pull-up resistor off on pins 0-7
	PORTG	=	0x00;									// Port G configured as logic low or pull-up resistor off on pins 0-7
	
	DDRD	&=	~(1 << DDD0);							// MCUI_SOURCE_SEL configured as input
	PORTD	|=	(1 << PORTD0);							// MCUI_SOURCE_SEL pull-up resistor enabled
	EICRA	|=	(1 << ISC01);							// MCUI_SOURCE_SEL configured for falling edge trigger
	EIMSK	|=	(1 << INT0);							// MCUI_SOURCE_SEL interrupt enabled
	
	DDRE	&=	~(1 << DDE4)   & ~(1 << DDE5)   & ~(1 << DDE6)   & ~(1 << DDE7);	// TEST_GPIO1 & TEST_GPIO2 configured as input
	PORTE	|=	 (1 << PORTE4) |  (1 << PORTE5) |  (1 << PORTE6) |  (1 << PORTE7);	// MCUI_SOURCE_SEL(A/B/C/D) pull-up resistor enabled
	EICRB	|=	 (1 << ISC41)  |  (1 << ISC51)  |  (1 << ISC61)  |  (1 << ISC71);	// MCUI_SOURCE_SEL(A/B/C/D) configured for falling edge trigger
	EIMSK	|=	 (1 << INT4)   |  (1 << INT5)   |  (1 << INT6)   |  (1 << INT7);	// MCUI_SOURCE_SEL(A/B/C/D) interrupt enabled
	
	DDRE	&=	~(1 << DDE0) & ~(1 << DDE1);			// TEST_RX & TEST_TX configured as input
	PORTE	|=	(1 << PORTE0) | (1 << PORTE1);			// TEST_RX & TEST_TX pull-up resistor enabled
			
	/* CONFIGURE DPOT SPI PORT */
	PORTD	|=	(1 << PORTD6);																		// Set DPOT_CS line
	UBRR1	=	0;																					// Set Baud Rate
	UCSR1C	=	(1 << UMSEL11) | (1 << UMSEL10) | (0 << UDORD1) | (1 << UCPHA1) | (1 << UCPOL1);	// Set MSPI mode of operation and SPI data mode 0 (Msb first, clock idle low, sample on rising edge)
	UCSR1B	=	(1 << TXEN1);																		// Enable transmitter
	UBRR1	=	1;																					// Set Baud Rate (Must be set after the transmitter is enabled)
	
	EIFR	=	0xFF;									// Clear all external interrupt flags
}


void led_toggle(void)
{
	PORTG	^=	(1 << PORTG0);								// MCU_LED_OUT 'toggle'
}


void led_on(void)
{
	PORTG	&=	~(1 << PORTG0);								// MCU_LED_OUT 'on'
}


void led_off(void)
{
	PORTG	|=	(1 << PORTG0);								// MCU_LED_OUT 'off'
}


void source_led_sel(unsigned char SOURCE_SEL)
{
	switch (SOURCE_SEL)
	{
		case 0:
		PORTF	&=	~(1 << PORTF0);								// MCU_LED_OUT 'on'
		PORTF	|=	 (1 << PORTF1);								// MCU_LED_OUT 'off'
		PORTF	|=	 (1 << PORTF2);								// MCU_LED_OUT 'off'
		PORTF	|=	 (1 << PORTF3);								// MCU_LED_OUT 'off'
			break;
		case 1:
		PORTF	|=	 (1 << PORTF0);								// MCU_LED_OUT 'off'
		PORTF	&=	~(1 << PORTF1);								// MCU_LED_OUT 'on'
		PORTF	|=	 (1 << PORTF2);								// MCU_LED_OUT 'off'
		PORTF	|=	 (1 << PORTF3);								// MCU_LED_OUT 'off'
			break;
		case 2:
		PORTF	|=	 (1 << PORTF0);								// MCU_LED_OUT 'off'
		PORTF	|=	 (1 << PORTF1);								// MCU_LED_OUT 'off'
		PORTF	&=	~(1 << PORTF2);								// MCU_LED_OUT 'on'
		PORTF	|=	 (1 << PORTF3);								// MCU_LED_OUT 'off'
			break;
		case 3:
		PORTF	|=	 (1 << PORTF0);								// MCU_LED_OUT 'off'
		PORTF	|=	 (1 << PORTF1);								// MCU_LED_OUT 'off'
		PORTF	|=	 (1 << PORTF2);								// MCU_LED_OUT 'off'
		PORTF	&=	~(1 << PORTF3);								// MCU_LED_OUT 'on'
			break;
		default:
			break;
	}
}

void source_led_on()
{
	PORTF	&=	 ~(1 << PORTF0);							// MCU_LED_OUT 'on'
	PORTF	&=	 ~(1 << PORTF1);							// MCU_LED_OUT 'on'
	PORTF	&=	 ~(1 << PORTF2);							// MCU_LED_OUT 'on'
	PORTF	&=	 ~(1 << PORTF3);							// MCU_LED_OUT 'on'
}


void source_led_off()
{
	PORTF	|=	 (1 << PORTF0);								// MCU_LED_OUT 'off'
	PORTF	|=	 (1 << PORTF1);								// MCU_LED_OUT 'off'
	PORTF	|=	 (1 << PORTF2);								// MCU_LED_OUT 'off'
	PORTF	|=	 (1 << PORTF3);								// MCU_LED_OUT 'off'
}


void dpot_set(unsigned char DPOT_POSITION)
{
	/* CLEAR CS LINE */
	PORTD			&=	~(1 << PORTD6);				// Clear DPOT_CS line
		
	for (int i = 0; i < 1000; i++)	{/* do nothing */}
	
	/* TRANSMIT ADDRESS FOR REGISTER B AND THEN TRANSMIT WIPER POSITION */
	UDR1 = 1;										// Start transmission
	while ( !( UCSR1A & (1<<UDRE1)) );				// Wait for transmission complete
	
	UDR1 = DPOT_POSITION;							// Start transmission
	while ( !( UCSR1A & (1<<UDRE1)) );				// Wait for transmission complete
	
	for (int i = 0; i < 1000; i++)	{/* do nothing */}
	
	/* SET CS LINE FOR DPOT1 OR DPOT2 */
	PORTD			|=	(1 << PORTD6);				// Set DPOT_CS line
}







void init_timer1(void)
{
	// Timer 1 Configuration
	GTCCR	|=	(1 << TSM);								// Activate Timer synchronization mode
	TCCR1B	|=	(1 << CS11) | (1 << CS10) | (1 << WGM12);		// Timer 1 clock select for divide by 64 and "CTC Mode"
	TCCR1A	=	0x00;									// Configure Timer 1 for normal port operation and "CTC Mode"
	OCR1A	=	RF_RATE_25MS;							// Configure the output capture and compare register to 25msec
	TCNT1	=	0x0000;									// Clear the Timer 1 count
	GTCCR	&=	~(1 << TSM);							// Exit Timer synchronization mode
}


void timer1_enable(unsigned char ENABLE)
{
	switch (ENABLE)
	{
		case 0:
			TCCR1B	=	0x00;											// Stop the Timer 1 clock
			TCNT1	=	0x0000;											// Clear the Timer 1 count
			break;
		case 1:
			TCCR1B	|=	(1 << CS11) | (1 << CS10) | (1 << WGM12);		// Timer 1 clock select for divide by 64 and "CTC Mode"
			TIMSK1	|=	(1 << TOIE1) | (1 << OCIE1A);					// Enable the Timer 1 overflow interrupt
			break;
		default:
			break;
	}
}
