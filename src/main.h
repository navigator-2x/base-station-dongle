/*
 * main.h
 *
 * Created: 5/12/2011 6:18:42 AM
 *  Author: Jeff Derderian
 */ 


#ifndef MAIN_H_
#define MAIN_H_

////////////////
//#define TESTMODE		// Define if performing Dongle OR Probe RF test
//#define TESTMODE_DONGLE	// Define if performing Dongle RF test ONLY
////////////////

/* Include Statements */

#include "compiler.h"
#include "avr/interrupt.h"
#include "avr/io.h"
#include "avr/sleep.h"
#include "avr/wdt.h"
#include "avr/eeprom.h"

#include "common_constants.h"
#include "Initialization/custom_functions.h"


/* Variable Declarations */

unsigned int	DONGLE_ID			= 0;

bool			RF_RX_END_ISR		= 0;
bool			RF_TX_END_ISR		= 0;
bool			TIMER_ISR			= 0;

bool			PROBE_CAL_SETTINGS_UPDATE	= 1;
bool			PROBE_CHANNEL_SW	= 0;
unsigned char	RF_RESEND_CAL_SETTINGS		= 0;
unsigned char   RF_RESEND_CH_UPDATE = 0;

unsigned char	MSG_TYPE			= 0;
unsigned char	MSG_DATA1			= 0;
unsigned char	MSG_DATA2			= 0;

unsigned char	SOURCE				= TC99;
unsigned char   SRC0_AMPLITUDE      = 25;   // TC-99    (1.136V) Upper Window    1.371 Lower Window    0.901
unsigned char   SRC1_AMPLITUDE      = 40;   // IN-111   (1.710V) Upper Window    2.374 Lower Window    1.045
unsigned char   SRC2_AMPLITUDE      = 75;   // I-131    (2.500V) Upper Window    3.255 Lower Window    2.255
unsigned char   SRC3_AMPLITUDE      = 7;    // I-125    (0.286V) Upper Window    0.342 Lower Window    0.230

bool			NEW_COUNT_MSG		= 0;
unsigned int	MSG_TYPE_ERROR		= 0x0000;
unsigned int	PULSE_COUNT			= 0x0000;
unsigned int	LAST_PULSE_COUNT	= 0x0000;
unsigned int	BITBANG_COUNT		= 0x0000;

bool			RF_GROUP_CHANGE		= 0;
bool			RF_CHANNEL_HOP		= 0;
unsigned char	TIMER_COUNT			= 0;
unsigned char	RF_GROUP			= 0;
unsigned char	RF_CHANNEL			= 15;
unsigned char	LQI					= 0;
unsigned char	CURRENT_RSSI		= 0;
unsigned int	RF_CHANNEL_TEST[27]	= {0};
	
bool			RF_ERROR			= 0;
unsigned int	RF_MSG_TYPE_ERROR	= 0;
unsigned int	RF_MSG_DATA1_ERROR	= 0;
unsigned int	RF_MSG_DATA2_ERROR	= 0;
unsigned int	BAD_PACKET			= 0;
unsigned int	MISSED_PACKET		= 0;

unsigned char	LED_BLINK_RATE		= 0;
unsigned char	LED_BLINK_RATE_MAX	= 50;


unsigned int	PULSE_BUFFER[]		= {'0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0'};
unsigned int	PULSE_BUFFER_SUM	= 0;
unsigned int	PULSE_BUFFER_AVG	= 0;
unsigned int	BAD_CHANNEL_COUNT	= 0;


/* Function Prototypes */
void source_sel(void);
void rf_rx(void);
void rf_tx(unsigned char MSG_TYPE, unsigned char MSG_DATA1, unsigned char MSG_DATA2);
void source_led_blink(unsigned char LED, unsigned char RATE);
void pulse_transfer(void);

#endif /* MAIN_H_ */